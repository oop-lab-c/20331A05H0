//Demonstrate different types of inheritance in Java.
package week5;
class GrandFather{
    GrandFather(){
        System.out.println("Grand father in base class");
    }
}
class GrandMother{
    GrandMother(){
        System.out.println("Grand mother in base class");
    }
}
//single inheritance and herirachial.
class Father extends GrandFather{
    Father(){
        System.out.println("Father inherited from grand father.");
    }
}
class Mother extends GrandFather{
    Mother(){
        System.out.println("Mother inherited from grand mother");
    }
}
//multiple inheritance cannot be achieved
//Multilevel Inheritance and hybrid
class Son extends Father{
    Son(){
        System.out.println("Son inherited from Father.");
    }
}
class InheriTypesJava {
    public static void main(String[] args) {
        System.out.println("Simple or single inheritance = ");
        new Father();                       // creating a  anonymous object withot reference
        System.out.println("\nMultilevel inheritance = ");
        new Son();
        System.out.println("\nHierarchial inheritance = ");    
        new Father();
        new Mother();
        System.out.println("\nHybrid inheritance = ");  
        new Father();
        new Mother();
        new Son();
    }    
}
