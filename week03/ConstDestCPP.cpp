/*Using a constructor and destructor in C++, Write a C++ class 'Student' and assign the 
following :
* string fullName
* int rollNum
* double semPerentage
* string collegeName
* int collegeCode*/

#include<iostream>
using namespace std;
class Student{
    static int count;
    public: 
    Student(string collegeN = "MVGR", int collegeC = 33 , double semPercentage = 90, int rollNo = 41, string name = "Ashik Ahamad" ){
        count++;
        cout<<"No. of objects created so far = "<<count<<endl;
        cout<<"Full name = "<<name<<endl;
        cout<<"Roll number = "<<rollNo<<endl;
        cout<<"Sem precentage = "<<semPercentage<<endl;
        cout<<"College Name = "<<collegeN<<endl;
        cout<<"College Code = "<<collegeC<<endl;
    }
    ~Student(){            //for destructor don't use ; 
        count--;
        cout<<"No. of object remaining after destructor = "<<count<<endl;
    }   
};
int Student :: count;
int main(int argc, char const *argv[])
{
    Student obj;   
    return 0;
}
