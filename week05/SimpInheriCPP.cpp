//Demonstrate Simple Inheritance using a real time example.

#include<iostream>
using namespace std;
class Templete{
    public:
    void wheel(){
        cout<<"A car has 4 wheels"<<endl;
    }
};
class car : public Templete{
    public :
    void design(){
        cout<<"Which is further designed like racing wheels"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    car obj;
    obj.wheel();
    obj.design();
    return 0;
}
