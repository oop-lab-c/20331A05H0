//Demonstrate method/function overriding in Java.
package week7;
class Base{
    void display(){
        System.out.println("Base class");
    }
}
class Derived extends Base{
    void display(){
        System.out.println("Derived class");
    }
}
class MethodORJava  {
    public static void main(String[] args) {
        Derived D = new Derived();
        D.display();
    }
}
