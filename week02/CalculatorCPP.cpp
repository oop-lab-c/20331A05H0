//Write a C++ program to compute arithmetic operations with the user input and choice of 
#include<iostream>
using namespace std;
int main(int argc, char const *argv[])
{
    cout<<"Enter two numbers = ";
    int a,b;
    cin>>a>>b;
    cout<<"Enter the operator (+,-,*,/,%)"<<endl;
    char op;
    cin>>op;
    switch(op){
        case '+' : cout<<"The sum = "<<a+b;break;
        case '-' : cout<<"The difference = "<<a-b;break;
        case '*' : cout<<"The product = "<<a*b;break;
        case '%' : cout<<"Remainder = "<<a%b;break;
        case '/' :if(b == 0){
            cout<<"Division not possible"<<endl;break;
        }
        else{
            cout<<"Quotient = "<<a/b<<endl;break;
        }
        default : cout<<"Invalid operator ";break;
    }
    return 0;
}