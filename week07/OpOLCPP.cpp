/*Demonstrate Operator overlaoding in C++.
*/
#include<iostream>
using namespace std;
class Binary{
    private:
    int a,b,c;
    public:
    void read(int x,int y, int z){
        a= x;
        b = y;
        c = z;
    }
    void display(){
        cout<<"a = "<<a<<"\tb = "<<b<<"\tc = "<<c<<endl;
    }
    Binary operator + (Binary b2){
        Binary b3;
        b3.a = a + b2.a;
        b3.b = b + b2.b;
        b3.c = b + b2.c;
        return b3;
    }
};
int main(int argc, char const *argv[])
{
    Binary b1, b2, b3;
    b1.read(1,2,3);
    cout<<"Object 1 memeber var = "<<endl;
    b1.display();
    b2.read(10,20,30);
    cout<<"Object 2 memeber var = "<<endl;
    b2.display();
    b3 = b1 + b2;       //b3 = b1.operator+(b2)
    cout<<"Addition of two objects (b3 result) = "<<endl;
    b3.display();
    return 0;
}
