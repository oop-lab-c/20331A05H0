package week9;

//Demonstrate creation of threads using runnable interface.
class MyThread implements Runnable{
    public void run(){
        for (int i=0; i<5; i++ ){
            System.out.println("Child Thread");
        }
    }
}
public class RunnableThreadJava {
    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        Thread t2 = new Thread(t1);
        t2.start();
        for(int i = 0; i<5; i++){
            System.out.println("Main Thread");
        }
    }
}
