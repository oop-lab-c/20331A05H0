/*Write a C++ class 'AccessSpecifierDemo' with the following members:
Member Variables :
1. private int priVar
2. protected int proVar
3. public int pubVar
Member Methods:
1. public void setVar(int priValue,int proValue, int pubValue)
2. public void getVar()
Assign values for each member variable(priVar,proVar,pubvar) and using methods
(setVar(),getVar()) and disaply them.
*/
#include<iostream>
using namespace std;
class Acces{
    private:
    int priVar; 
    protected :
    int proVar;   
    public:
    int pubVar;
    void setVar(int priVar, int proVar, int pubVar){
        this->priVar = priVar;
        this->proVar = proVar;
        this->pubVar = pubVar;
    }
    int getVar(){  // we cannot use two function name with same signeture in a class
        return priVar;
    }
    int getVars(){
        return proVar;
    }
};
int main(){
    Acces a;
    a.setVar(10,20,30);
    cout<<"Private var = "<<a.getVar()<<endl;
    cout<<"Protected var = "<<a.getVars()<<endl;
    cout<<"Public var = "<<a.pubVar<<endl;
    return 0;
}