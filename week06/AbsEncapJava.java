/*Write a Java class 'AccessSpecifierDemo' with the following members:
Member Variables :
1. private int priVar
2. protected int proVar
3. public int pubVar
Member Methods:
1. public void setVar(int priValue,int proValue, int pubValue)
2. public void getVar()
Assign values for each member variable(priVar,proVar,pubvar) and using methods
(setVar(),getVar()) and disaply them.
 */
package week6;
class Acces{
    private int priVar ;
    protected int proVar;
    int pubVar ;
    void setVar(int priVar, int proVar, int pubVar){
        this.priVar = priVar;  
        this.proVar = proVar;
        this.pubVar = pubVar;
    }
    int getVar(){  // we cannot use two function name with same signeture in a class
        return priVar;
    }
    int getVars(){
        return proVar;
    }
}
public class AbsEncapJava {
    public static void main(String[] args) {
        Acces a = new Acces();
        a.setVar(10,20,30);
        System.out.println("Private var = "+a.getVar());
        System.out.println("Protected var = "+a.getVars());
        System.out.println("Public var = "+a.pubVar);
    }
}
