//Demonstrate partial abstraction in Java.
package week7;
abstract class Animal{
    abstract void walk();
    Animal(){
        System.out.println("Created a new animal");
    }
    public void eat(){
        System.out.println("Animal eats");
    }
}
class Horse extends Animal{
    public void walk(){
        System.out.println("Walks with 4 legs");
    }
    Horse(){
        System.out.println("Animal name is dog");
    }
}
class PartialAbstraction {
    public static void main(String[] args) {
        Horse h = new Horse();
        h.eat();
        h.walk(); 
    }
}
