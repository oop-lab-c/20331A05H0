//Demonstrate different types of inheritance in C++.

#include<iostream>
using namespace std;
// base class
class GrandFather{
    public:
    GrandFather(){
        cout<<"Grand father in base class"<<endl;
    }
}; //put coma after the end of class
class GrandMother{
    public:
    GrandMother(){
        cout<<"Grand mother in base class"<<endl;
    }
};
//single inheritance and herirachial
class Father : public GrandFather{
    public:
    Father(){
        cout<<"Father inherited from grand father."<<endl;
    }
};
class Mother : public GrandFather{
    public:
    Mother(){
        cout<<"Mother inherited from grand mother"<<endl;
    }
};
//Multilevel Inheritance and hierarchial inheritance
class Son : public Father{
    public:
    Son(){
        cout<<"Son inherited from Father."<<endl;
    }
};
//Multiple inheritance
class Uncle : public GrandFather, GrandMother{
    public:
    Uncle(){
        cout<<"Uncle inherited from GrandFather, GrandMother"<<endl;
    }
};
//Hybrid inheritance
class GrandDaughter : public Uncle{ 
    public:
    GrandDaughter(){
        cout<<"Grand Daughter inherited from Uncle"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    cout<<"Simple or single inheritance = "<<endl;
    Father o;
    cout<<"\nMultilevel inheritance = "<<endl;
    Uncle o1;
    cout<<"\nHierarchial inheritance = "<<endl;
    Father o2;
    Mother o3;
    cout<<"\nMultiple inheritance = "<<endl;
    Son o4;
    cout<<"\nHybrid inheritance = "<<endl;
    GrandDaughter o5;
    return 0;
}
/*note : gf
          |
          f
          |\
          s-gd
in multilevel inheritance you can directly inherit or access the predisiscor class not the anscestor class*/