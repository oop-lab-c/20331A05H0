//Demonstrate partial abstraction in C++.
#include<iostream>
using namespace std;
class base{
    public:
    void display(){
        cout<<"display function in base class"<<endl;
    }
    virtual void show(){}
};
class derived : public base{
    public:
    void show(){
        cout<<"Show function in Derived class"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    derived obj;
    obj.display();
    obj.show();
    return 0;
}
