//Demonstrate pure abstraction using interfaces in Java.
package week7;
interface A{
    int a = 10;
    void show();
}
interface B{
    int a = 20;
    void show();
}
class C implements A,B{
    public void show(){
        System.out.println("A interface var a = "+A.a);
        System.out.println("B interface var a = "+B.a);
    }
}
class PureAbsJava{
    public static void main(String[] args) {
        C obj = new C();
        obj.show();
    }
}
