package week9;
//Demonstrate creation of threads using Thread class.
class MyThread extends Thread{
    public void run(){
        for(int i = 0; i<5; i++){
            System.out.println("Child thread");
        }
    }
}
class ThreadClassJava {
    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        t1.start();
        for (int i = 0; i<5 ; i++){
            System.out.println("Main Thread");
        }
    }
}
//Exception in thread "main" java.lang.NoSuchMethodError: week9.MyThread: method 'void <init>()' not found
//at week9.ThreadClassJava.main(ThreadClassJava.java:11)