//Demonstrate Simple Inheritance using a real time example.
package week5;
class Templete{
    void wheel(){
        System.out.println("A car has 4 wheels");
    }
}
class car extends Templete{
    void design(){
        System.out.println("Which is further designed like racing wheels");
    }
}
class SimpInheriJava {
    public static void main(String[] args) {
        car obj = new car();
        obj.wheel();
        obj.design();
    }
}