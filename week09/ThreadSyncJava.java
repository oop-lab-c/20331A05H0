package week9;
class Hello{
    synchronized void hello(String name){
        for (int i = 0; i<5; i++){
            System.out.println("hai"+name);
        }
    }
}
class MyThread  extends Thread{
    Hello z;
    String name;
    MyThread(Hello h , String name){
        z = h;
        this.name = name;
    }
    public void run(){
        z.hello(name);
    }
}
public class ThreadSyncJava{
    public static void main(String[] args) {
        Hello h = new Hello();
        MyThread t1 = new MyThread(h,"Ashik");
        MyThread t2 = new MyThread(h,"Mahesha");
        t1.start();
        t2.start();
    }
}