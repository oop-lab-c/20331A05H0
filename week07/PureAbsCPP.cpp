//Demonstrate pure abstraction using virtual functions in C++.
#include<iostream>
using namespace std;
class shape{
    public:
    virtual void area()=0;
};
class circle : public shape{
    public:
    void area(){
        cout<<"Area of circle"<<endl;
    }
};
class rectangle : public shape{
    public:
    void area(){
        cout<<"Area of rectangle"<<endl;
    }
};
int main(){
    shape *b;
    circle obj;
    b = &obj;
    b->area();
    rectangle o;
    b = &o;
    b->area();
    return 0;
}