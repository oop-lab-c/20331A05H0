//Demonstrate Template Functions and classes in C++.
#include<iostream>
using namespace std;
template<class T1, class T2>
void displayfn(T1 a, T2 b){
    cout<<"The function templete = "<<endl<<"a = "<<a<<"\tb = "<<b<<endl;
}
template<class T>
void show(T a){
    cout<<"Choosable Value in templete function a is ="<<a<<endl;
}
void show(int a){
    cout<<"Direct value of a is ="<<a<<endl;
}
template<class T1, class T2>
class Test{
    private: T1 a ;  T2 b;
    public: Test(T1 a,T2 b){
        this->a = a;
        this->b = b;
    }
    void displayC(){
        cout<<"The class templete = "<<endl<<"a = "<<a<<"\tb = "<<b<<endl;
    }
    void showClass();
};
//calling fun using scope resolution op
template<class T2, class T1> // you can use any order of T1...2 var in that scope regardless of class fun using scope resolution or not.
void Test<T2, T1> :: showClass(){ // but in that scope it should be in same order.
    cout<<"Template fun inside a class and defined out of class"<<endl<<"a = "<<a<<"\tb = "<<b<<endl;
}
int main(int argc, char const *argv[])
{
    //function template
    displayfn(10,20);
    displayfn(1,2.2);
    displayfn('a',3.2);
    //class template
    Test<int,int> obj1(1,2);
    obj1.displayC();
    Test<char,double> o('c',2.2);
    o.displayC();
    //experiment
    show(10);
    //scope resolution
    Test<int, int> ob(3,9);
    ob.showClass();
    return 0;
}
//note :1. u have to specify the template just before the scope
// 2. Exact match takes highest priority
