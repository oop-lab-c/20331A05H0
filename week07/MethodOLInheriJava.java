//Demonstrate method/function overlaoding using inheritance in Java.

package week7;
class MethodO{
    int add(int a, int b){
        return a+b;
    }
    double add(double a, double b){
        return a+b;
    }
    int add(int a,int b, int c){
        return a+b+c;
    }
}
class Add extends MethodO{}
public class MethodOLInheriJava {
    public static void main(String[] args) {
        Add m = new Add();
        System.out.println("The sum = "+m.add(10,20));
        System.out.println("The sum = "+m.add(2.2,3.3));
        System.out.println("The sum = "+m.add(10,20,30));
    }    
}
