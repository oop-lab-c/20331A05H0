/*Demonstrate method/function overlaoding in C++.*/
#include<iostream>
using namespace std;
class Method {
    public:
    int add(int a, int b){
       return (a+b);
    }
   // void add(int a, int b){            //overload cannot be performed based on return type
     //   cout<<"The sum"<<a+b<<endl;
    //}
    double add(double a, double b){
        return a+b;
    }
    int add(int a,int b ,int c){
        return (a+b+c);
    }
    float add (float a, float b ){ //float and double are same but diff in length
        return a+b;
    }
};
int main(int argc, char const *argv[])
{
    Method obj;
    cout<<"The sum = "<<obj.add(10,20)<<endl;
    //obj.add(10,20);
    cout<<"The sum = "<<obj.add(10,20,30)<<endl;
    cout<<"The sum = "<<obj.add(1.1,2.2)<<endl;
    cout<<"The sum = "<<obj.add(2.2f,3.3f)<<endl; // if you not use 'f ' compailer think it a double and returns error
    return 0;
}
