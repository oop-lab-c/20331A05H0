package week8;
//Demonstrate user defined exceptions in java with finally block.
//import java.lang.Exception;
import java.util.Scanner;
class AgeError extends Exception{
    AgeError(){
        super("Age is not eligible to vote");
    }
}
class UserExHandJava{
    public static void main(String[] args) {
        System.out.println("Enter the age = ");
        Scanner sc  =  new Scanner(System.in);
        int  a = sc.nextInt();
        try{
            if(a < 18){
                throw new AgeError();
            }
            else{
                System.out.println("Your eligible to vote.");
            }
        }
        catch(AgeError e){
            System.out.println(e.toString());
            e.printStackTrace();
        }
        finally{
            System.out.println("Your are eligible to vote after "+ (18-a) + " years");
        }

    }
}
//note : if program is terminate abnormally without catching it will execute the finally block
 